The REST API Plugin provides the ability to manage Serveria servers by sending an REST/HTTP request to the server. This plugin’s functionality is useful for applications that need to administer Serveria servers outside of the Serveria admin console.

REST API Plugin Readme
Feature list
Installation
Explanation of REST
Authentication
Basic HTTP Authentication
Shared secret key
Data format
Data types
User
System Property
User related REST Endpoints
Retrieve users
Possible parameters
Examples
Retrieve a user
Possible parameters
Examples
Create a user
Examples
XML Examples
JSON Examples
Delete a user
Possible parameters
Examples
Update a user
Possible parameters
Examples
XML Example
Rename Example
JSON Example
Retrieve all user groups
Possible parameters
Examples
Add user to group
Possible parameters
Examples
Delete a user from a group
Possible parameters
Examples
Lockout a user
Possible parameters
Examples
Unlock a user
Possible parameters
Examples
System related REST Endpoints
Retrieve all system properties
Examples
Retrieve system property
Possible parameters
Examples
Create a system property
Examples
Delete a system property
Possible parameters
Examples
Update a system property
Possible parameters
Examples
(Deprecated) User Service Plugin Readme
Overview
Installation
Configuration
Using the Plugin
Sample HTML
Server Reply
Feature list
Lockout or unlock the user (enable / disable)
Get overview over all or specific system properties and to create, update or delete system property
Get overview over all or specific chat room and to create, update or delete a chat room

Installation
Copy restAPI.jar into the plugins directory of your Serveria server. The plugin will then be automatically deployed. To upgrade to a new version, copy the new restAPI.jar file over the existing file.

Explanation of REST
To provide a standard way of accessing the data the plugin is using REST.

HTTP Method	Usage
GET	Receive a read-only data
PUT	Overwrite an existing resource
POST	Creates a new resource
DELETE	Deletes the given resource
Authentication
All REST Endpoint are secured and must be authenticated. There are two ways to authenticate:

Basic HTTP Authentication
Shared secret key
The configuration can be done in Serveria Admin console under Server > Server Settings > REST API.

Basic HTTP Authentication
To access the endpoints is that required to send the Username and Password of a Serveria Admin account in your HTTP header request.

E.g. Header: Authorization: Basic YWRtaW46MTIzNDU= (username: admin / password: 12345)

Shared secret key
To access the endpoints is that required to send the secret key in your header request. The secret key can be defined in Serveria Admin console under Server > Server Settings > REST API.

E.g. Header: Authorization: s3cretKey

Data format
Openfire REST API provides XML and JSON as data format. The default data format is XML.
To get a JSON result, please add “Accept application/json” to the request header.
If you want to create a resource with JSON data format, please add “Content-Type: application/json“.

Data types
User
Parameter	Optional	Description
username	No	The username of the user
name	Yes	The name of the user
email	Yes	The email of the user
password	No	The password of the user
properties	Yes	List of properties. Property is a key / value object. The key must to be per user unique

User related REST Endpoints
Retrieve users
Endpoint to get all or filtered users

GET /users

Payload: none
Return value: Users

Possible parameters
Parameter	Parameter Type	Description	Default value
search	@QueryParam	Search/Filter by username.
This act like the wildcard search %String%	
propertyKey	@QueryParam	Filter by user propertyKey.	
propertyValue	@QueryParam	Filter by user propertyKey and propertyValue.
Note: It can only be used within propertyKey parameter	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

GET http://example.org:9090/plugins/restapi/v1/users
GET http://example.org:9090/plugins/restapi/v1/users?search=testuser
GET http://example.org:9090/plugins/restapi/v1/users?propertyKey=keyname
GET http://example.org:9090/plugins/restapi/v1/users?propertyKey=keyname&propertyValue=keyvalue

If you want to get a JSON format result, please add “Accept: application/json” to the Header.

Retrieve a user
Endpoint to get information over a specific user

GET /users/{username}

Payload: none
Return value: User

Possible parameters
Parameter	Parameter Type	Description	Default value
username	@Path	Exact username	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

GET http://example.org:9090/plugins/restapi/v1/users/testuser

Create a user
Endpoint to create a new user

POST /users

Payload: User
Return value: HTTP status 201 (Created)

Examples
XML Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type: application/xml

POST http://example.org:9090/plugins/restapi/v1/users

Payload Example 1 (required parameters):

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<user>
    <username>test3</username>
    <password>p4ssword</password>
</user>
Payload Example 2 (available parameters):

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<user>
    <username>testuser</username>
    <password>p4ssword</password>
    <name>Test User</name>
    <email>test@localhost.de</email>
    <properties>
        <property key="keyname" value="value"/>
        <property key="anotherkey" value="value"/>
    </properties>
</user>
JSON Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type: application/json

POST http://example.org:9090/plugins/restapi/v1/users

Payload Example 1 (required parameters):

{
    "username": "admin",
    "password": "p4ssword"
}
Payload Example 2 (available parameters):

{
    "username": "admin",
    "password": "p4ssword",
    "name": "Administrator",
    "email": "admin@example.com",
    "properties": {
        "property": [
            {
                "@key": "console.rows_per_page",
                "@value": "user-summary=8"
            },
            {
                "@key": "console.order",
                "@value": "session-summary=1"
            }
        ]
    }
}
Delete a user
Endpoint to delete a user

DELETE /users/{username}

Payload: none
Return value: HTTP status 200 (OK)

Possible parameters
Parameter	Parameter Type	Description	Default value
username	@Path	Exact username	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
DELETE http://example.org:9090/plugins/restapi/v1/users/testuser

Update a user
Endpoint to update / rename a user

PUT /users/{username}

Payload: User
Return value: HTTP status 200 (OK)

Possible parameters
Parameter	Parameter Type	Description	Default value
username	@Path	Exact username	
Examples
XML Example
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type application/xml

PUT http://example.org:9090/plugins/restapi/v1/users/testuser

Payload:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<user>
    <username>testuser</username>
    <name>Test User edit</name>
    <email>test@edit.de</email>
    <properties>
        <property key="keyname" value="value"/>
    </properties>
</user>
Rename Example
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type application/xml

PUT http://example.org:9090/plugins/restapi/v1/users/oldUsername

Payload:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<user>
    <username>newUsername</username>
    <name>Test User edit</name>
    <email>test@edit.de</email>
    <properties>
        <property key="keyname" value="value"/>
    </properties>
</user>
JSON Example
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type application/json

PUT http://example.org:9090/plugins/restapi/v1/users/testuser

Payload:

{
    "username": "testuser",
    "name": "Test User edit",
    "email": "test@edit.de",
    "properties": {
        "property": {
            "@key": "keyname",
            "@value": "value"
        }
    }
}
Retrieve all user groups
Endpoint to get group names of a specific user

GET /users/{username}/groups

Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type application/xml

DELETE http://example.org:9090/plugins/restapi/v1/chatrooms/global/owners/testUser
DELETE http://example.org:9090/plugins/restapi/v1/chatrooms/global/owners/testUser@openfire.com
DELETE http://example.org:9090/plugins/restapi/v1/chatrooms/global/admins/testUser
DELETE http://example.org:9090/plugins/restapi/v1/chatrooms/global/members/testUser
DELETE http://example.org:9090/plugins/restapi/v1/chatrooms/global/outcasts/testUser
DELETE http://example.org:9090/plugins/restapi/v1/chatrooms/global/owners/testUser?servicename=privateconf

System related REST Endpoints
Retrieve all system properties
Endpoint to get all system properties

GET /system/properties

Payload: none
Return value: System properties

Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

GET http://example.org:9090/plugins/restapi/v1/system/properties

Retrieve system property
Endpoint to get information over specific system property

GET /system/properties/{propertyName}

Payload: none
Return value: System property

Possible parameters
Parameter	Parameter Type	Description	Default value
propertyName	@Path	The name of system property	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

GET http://example.org:9090/plugins/restapi/v1/system/properties/xmpp.domain

Create a system property
Endpoint to create a system property

POST system/properties

Payload: System Property
Return value: HTTP status 201 (Created)

Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type: application/xml

POST http://example.org:9090/plugins/restapi/v1/system/properties

Payload Example:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<property key="propertyName" value="propertyValue"/>
Delete a system property
Endpoint to delete a system property

DELETE /system/properties/{propertyName}

Payload: none
Return value: HTTP status 200 (OK)

Possible parameters
Parameter	Parameter Type	Description	Default value
propertyName	@Path	The name of system property	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

DELETE http://example.org:9090/plugins/restapi/v1/system/properties/propertyName

Update a system property
Endpoint to update / overwrite a system property

PUT /system/properties/{propertyName}

Payload: System property
Return value: HTTP status 200 (OK)

Possible parameters
Parameter	Parameter Type	Description	Default value
propertyName	@Path	The name of system property	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type application/xml

PUT http://example.org:9090/plugins/restapi/v1/system/properties/propertyName

Payload:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<property key="propertyName" value="anotherValue"/>
Group related REST Endpoints
Retrieve all groups
Endpoint to get all groups

GET /groups

Payload: none
Return value: Groups

Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

GET http://example.org:9090/plugins/restapi/v1/groups

Retrieve a group
Endpoint to get information over specific group

GET /groups/{groupName}

Payload: none
Return value: Group

Possible parameters
Parameter	Parameter Type	Description	Default value
groupName	@Path	The name of the group	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

GET http://example.org:9090/plugins/restapi/v1/groups/moderators

Create a group
Endpoint to create a new group

POST /groups

Payload: Group
Return value: HTTP status 201 (Created)

Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type: application/xml

POST http://example.org:9090/plugins/restapi/v1/groups

Payload Example:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<group>
    <name>GroupName</name>
    <description>Some description</description>
</group>
Delete a group
Endpoint to delete a group

DELETE /groups/{groupName}

Payload: none
Return value: HTTP status 200 (OK)

Possible parameters
Parameter	Parameter Type	Description	Default value
groupName	@Path	The name of the group	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=

DELETE http://example.org:9090/plugins/restapi/v1/groups/groupToDelete

Update a group
Endpoint to update / overwrite a group

PUT /groups/{groupName}

Payload: Group
Return value: HTTP status 200 (OK)

Possible parameters
Parameter	Parameter Type	Description	Default value
groupName	@Path	The name of the group	
Examples
Header: Authorization: Basic YWRtaW46MTIzNDU=
Header: Content-Type application/xml

PUT http://example.org:9090/plugins/restapi/v1/groups/groupNameToUpdate

Payload:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<group>
    <name>groupNameToUpdate</name>
    <description>New description</description>
</group>
(Deprecated) User Service Plugin Readme
Overview
The User Service Plugin provides the ability to add,edit,delete users and manage their rosters by sending an http request to the server. It is intended to be used by applications automating the user administration process. This plugin’s functionality is useful for applications that need to administer users outside of the Openfire admin console. An example of such an application might be a live sports reporting application that uses XMPP as its transport, and creates/deletes users according to the receipt, or non receipt, of a subscription fee.

Installation
Copy userservice.jar into the plugins directory of your Openfire server. The plugin will then be automatically deployed. To upgrade to a new version, copy the new userservice.jar file over the existing file.

Configuration
Access to the service is restricted with a “secret” that can be viewed and set from the User Service page in the Openfire admin console. This page is located on the admin console under “Server” and then “Server Settings”. This should really only be considered weak security. The plugin was initially written with the assumption that http access to the Openfire service was only available to trusted machines. In the case of the plugin’s author, a web application running on the same server as Openfire makes the request.

Using the Plugin
To administer users, submit HTTP requests to the userservice service. The service address is [hostname]plugins/restapi/userservice. For example, if your server name is “example.com”, the URL is http://example.com/plugins/restapi/userservice

The following parameters can be passed into the request:

Name		Description
type	Required	The admin service required. Possible values are ‘add’, ‘delete’, ‘update’, ‘enable’, ‘disable’, ‘add_roster’, ‘update_roster’, ‘delete_roster’, ‘grouplist’, ‘usergrouplist’.
secret	Required	The secret key that allows access to the User Service.
username	Required	The username of the user to ‘add’, ‘delete’, ‘update’, ‘enable’, ‘disable’, ‘add_roster’, ‘update_roster’, ‘delete_roster’. ie the part before the @ symbol.
password	Required for ‘add’ operation	The password of the new user or the user being updated.
name	Optional	The display name of the new user or the user being updated. For ‘add_roster’, ‘update_roster’ operations specifies the nickname of the roster item.
email	Optional	The email address of the new user or the user being updated.
groups	Optional	List of groups where the user is a member. Values are comma delimited. When used with types “add” or “update”, it adds the user to shared groups and auto-creates new groups. When used with ‘add_roster’ and ‘update_roster’, it adds the user to roster groups provided the group name does not clash with an existing shared group.
item_jid	Required for ‘add_roster’, ‘update_roster’, ‘delete_roster’ operations.	The JID of the roster item
subscription	Optional	Type of subscription for ‘add_roster’, ‘update_roster’ operations. Possible numeric values are: -1(remove), 0(none), 1(to), 2(from), 3(both).
Sample HTML
The following example adds a user

http://example.com:9090/plugins/restapi/userservice?type=add&secret=bigsecret&username=kafka&password=drowssap&name=franz&email=franz@kafka.com

The following example adds a user, adds two shared groups (if not existing) and adds the user to both groups.

http://example.com:9090/plugins/restapi/userservice?type=add&secret=bigsecret&username=kafka&password=drowssap&name=franz&email=franz@kafka.com&groups=support,finance

The following example deletes a user and all roster items of the user.

http://example.com:9090/plugins/restapi/userservice?type=delete&secret=bigsecret&username=kafka

The following example disables a user (lockout)

http://example.com:9090/plugins/restapi/userservice?type=disable&secret=bigsecret&username=kafka

The following example enables a user (removes lockout)

http://example.com:9090/plugins/restapi/userservice?type=enable&secret=bigsecret&username=kafka

The following example updates a user

http://example.com:9090/plugins/restapi/userservice?type=update&secret=bigsecret&username=kafka&password=drowssap&name=franz&email=beetle@kafka.com

The following example adds new roster item with subscription ‘both’ for user ‘kafka’

http://example.com:9090/plugins/restapi/userservice?type=add_roster&secret=bigsecret&username=kafka&item_jid=franz@example.com&name=franz&subscription=3

The following example adds new roster item with subscription ‘both’ for user ‘kafka’ and adds kafka to roster groups ‘family’ and ‘friends’

http://example.com:9090/plugins/restapi/userservice?type=add_roster&secret=bigsecret&username=kafka&item_jid=franz@example.com&name=franz&subscription=3&groups=family,friends

The following example updates existing roster item to subscription ‘none’ for user ‘kafka’

http://example.com:9090/plugins/restapi/userservice?type=update_roster&secret=bigsecret&username=kafka&item_jid=franz@example.com&name=franz&subscription=0

The following example deletes a specific roster item ‘franz@kafka.com’ for user ‘kafka’

http://example.com:9090/plugins/restapi/userservice?type=delete_roster&secret=bigsecret&username=kafka&item_jid=franz@example.com

The following example gets all groups

http://example.com:9090/plugins/restapi/userservice?type=grouplist&secret=bigsecret
Which replies an XML group list formatted like this:

<result>
    <groupname>group1</groupname>
    <groupname>group2</groupname>
</result>
The following example gets all groups for a specific user

http://example.com:9090/plugins/restapi/userservice?type=usergrouplist&secret=bigsecret&username=kafka
Which replies an XML group list formatted like this:

<result>
    <groupname>usergroup1</groupname>
    <groupname>usergroup2</groupname>
</result>
* When sending double characters (Chinese/Japanese/Korean etc) you should URLEncode the string as utf8.
In Java this is done like this
URLEncoder.encode(username, “UTF-8”));
If the strings are encoded incorrectly, double byte characters will look garbeled in the Admin Console.

Server Reply
The server will reply to all User Service requests with an XML result page. If the request was processed successfully the return will be a “result” element with a text body of “OK”, or an XML grouplist formatted like in the example for “grouplist” and “usergrouplist” above. If the request was unsuccessful, the return will be an “error” element with a text body of one of the following error strings.

Error String	Description
IllegalArgumentException	One of the parameters passed in to the User Service was bad.
UserNotFoundException	No user of the name specified, for a delete or update operation, exists on this server. For ‘update_roster’ operation, roster item to be updated was not found.
UserAlreadyExistsException	A user with the same name as the user about to be added, already exists. For ‘add_roster’ operation, roster item with the same JID already exists.
RequestNotAuthorised	The supplied secret does not match the secret specified in the Admin Console or the requester is not a valid IP address.
UserServiceDisabled	The User Service is currently set to disabled in the Admin Console.
SharedGroupException	Roster item can not be added/deleted to/from a shared group for operations with roster.

More info and documentation at https://serveria.com
