# README #

The REST API Plugin provides the ability to manage Serveria servers by sending an REST/HTTP request to the server. This plugin’s functionality is useful for applications that need to administer Serveria servers outside of the Serveria admin console.

https://serveria.com